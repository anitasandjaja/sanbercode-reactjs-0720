// soal1
console.log('LOOPING PERTAMA');

var flag = 2;
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  flag +=2 ; // Mengubah nilai flag dengan menambahkan 1
}
console.log('LOOPING KEDUA');

// var flag = 20;
while(flag >= 2) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  flag -=2 ; // Mengubah nilai flag dengan menambahkan 1
}

// soal2
for(i=0;i<=20;i++){
  if((i%3==0) && (i%2==1)){
    console.log(i + ' - I love coding')
  }
  else if(i%2==1){
    console.log(i + ' - Santai');
  }else if(i%2==0){
    console.log(i + ' - Berkualitas')
  }else{
    console.log(' ');
  }
}

// soal3

    var s = ''; 
    for (var i=1;i<=7;i++){
      for(var j=1;j<i;j++){
        s += '*';
      }
      s+= '\n';
    }
    console.log(s);
// soal4
var kalimat="saya sangat senang belajar javascript";
var kalimat2 = kalimat.split(" ");
console.log(kalimat2);
// // soal5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var urut = daftarBuah.sort();
for (i in urut){
    console.log(urut[i]);
}

